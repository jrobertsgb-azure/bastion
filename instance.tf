resource "azurerm_virtual_machine" "bastion" {
  name                  = "bastion"
  location              = "${data.terraform_remote_state.core.location}"
  resource_group_name   = "${data.terraform_remote_state.core.name}"
  network_interface_ids = ["${azurerm_network_interface.bastion.id}"]
  delete_os_disk_on_termination = true

  vm_size = "Standard_B1s"

  storage_image_reference  {
    publisher = "CoreOS"
    offer     = "CoreOS"
    sku       = "Stable"
    version   = "latest"
  }

  storage_os_disk {
    name              = "bastion"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name = "bastion"
    admin_username       = "core"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/core/.ssh/authorized_keys"
      key_data = "${data.terraform_remote_state.core.public_key}"
    }
  }
}
