terraform {
  backend "azurerm" {
    storage_account_name = "jrobertsterraform"
    container_name       = "terraform"
    key                  = "bastion"
  }
}