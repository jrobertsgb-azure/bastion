data "terraform_remote_state" "core" {
  backend = "azurerm"

  config {
    storage_account_name = "jrobertsterraform"
    container_name       = "terraform"
    key                  = "core"
  }
}
