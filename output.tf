output "host" {
    value = "${cloudflare_record.dns.hostname}"
}

output "ip_address" {
  value = "${azurerm_public_ip.bastion.ip_address}"
}
